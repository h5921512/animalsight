using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireArrowani : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip clip;
    public AudioSource audioSource;
    public int numberArrow = 1;
    string playani = "FireArrow";
    
    bool isplayed = false;
    private void OnTriggerEnter(Collider other)
    {
        if (!isplayed)
        {
            gameObject.GetComponent<Animator>().Play("FireArrow" + numberArrow);
            audioSource.PlayOneShot(clip);
            Destroy(gameObject, 2);
            isplayed = true;
        }
    }
  

}
