using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
[RequireComponent(typeof(ActionBasedController))]
public class HandController : MonoBehaviour
{
    ActionBasedController controller;

    public Hand hand;
    public int zoom = 20;
    public int normal = 60;
    float smooth = 5;
  
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<ActionBasedController>();
       
    }

    // Update is called once per frame
    void Update()
    {
        hand.SetGrip(controller.selectAction.action.ReadValue<float>());
        if (controller.selectAction.action.ReadValue<float>() > 0.01)
        {
            Debug.Log("using");
            
        }
        hand.SetTrigger(controller.activateAction.action.ReadValue<float>());
        
    }
}
