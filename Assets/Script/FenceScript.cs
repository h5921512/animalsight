using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FenceScript : MonoBehaviour
{

    bool isplayed=false;
    public GameObject troch1;
    public GameObject troch2;
    public GameObject blackplane;
    public AudioSource player;
    public AudioClip clip;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (!isplayed)
        {
            gameObject.GetComponent<Animator>().Play("Fence");
            troch1.SetActive(true);
            troch2.SetActive(true);
            blackplane.SetActive(false);
            player.PlayOneShot(clip);
        }

    }
}
