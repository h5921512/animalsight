using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
public class CharaCterHealth : MonoBehaviour
{
   
    public int CharacterHealthis = 35;
    public AudioClip clip;
    public AudioSource audioSource;
    public GameObject bloodVolume;

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "trap")
        {

            CharacterHealthis--;
            Debug.Log("hurt");
            Destroy(other.gameObject);
            audioSource.PlayOneShot(clip);
            bloodVolume.SetActive(true);
            HapticPulseUnity();
            Invoke("cancelbloodeffect",0.15f);
        }
        else if (other.gameObject.tag == "sdab")
        {
            audioSource.PlayOneShot(clip);
            
            CharacterHealthis--;
            bloodVolume.SetActive(true);
            HapticPulseUnity();           
            Invoke("cancelbloodeffect", 0.15f);
        }
        if (CharacterHealthis == 0)
        {
            SceneManager.LoadScene(1);
        }
    }
    void HapticPulseUnity()
    {
        InputDevice device = InputDevices.GetDeviceAtXRNode(XRNode.RightHand);
        HapticCapabilities capabilities;
        if (device.TryGetHapticCapabilities(out capabilities))
            if (capabilities.supportsImpulse)
                device.SendHapticImpulse(0, 1.0f, 0.3f);

        device = InputDevices.GetDeviceAtXRNode(XRNode.LeftHand);
        if (device.TryGetHapticCapabilities(out capabilities))
            if (capabilities.supportsImpulse)
                device.SendHapticImpulse(0, 1.0f, 0.3f);
    }
    void cancelbloodeffect()
    {
        bloodVolume.SetActive(false);
    }
}
