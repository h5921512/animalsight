using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMode : MonoBehaviour
{
    public Material[] mats;
    Renderer rend;
    int mode = 0;   
    // Start is called before the first frame update
    void Start()
    {
        mode = 0;
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = mats[mode];
    }

    // Update is called once per frame
    public void ChangedColor()
    {
        if (mode == 0)
        {
            rend.sharedMaterial = mats[mode];
            mode++;
        }
        else if (mode == 1)
        {
            rend.sharedMaterial = mats[mode];
            mode++;
        }
        else if (mode == 2)
        {
            rend.sharedMaterial = mats[mode];
            mode = 0;
        }
    }
}
