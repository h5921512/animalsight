using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class Roomani : MonoBehaviour
{
      
    public AudioSource audioSource;
    public AudioClip clip;
    bool isplayed = false;
    public bool FirstCode = false;
    public bool SecondeCode = false;
    public GameObject DoorCodes;
    bool isentered = false;
    public void TestFunction()
    {
        if (!isplayed)
        {
            gameObject.GetComponent<Animator>().Play("Door");
            audioSource.PlayOneShot(clip);
            HapticPulseUnity();
            isplayed = true;
        }
    }

    public void WrongCode()
    {
        FirstCode = false;
        SecondeCode = false;
    }

    public void isAnswer()
    {
        if (FirstCode == true && SecondeCode == true)
        {
            DoorCodes.SetActive(false);
            gameObject.GetComponent<Animator>().Play("DoorClose");
            audioSource.PlayOneShot(clip);
            HapticPulseUnity();
        }
        else 
            return;
    }
    public void FirstCodeAns()
    {
        FirstCode = true;
        isAnswer();
    }
    public void SecondCodeAns()
    {
        SecondeCode = true;
        isAnswer();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!isentered&&other.tag=="Player")
        {
            gameObject.GetComponent<Animator>().Play("Door");
            audioSource.PlayOneShot(clip);
            isentered = true;
            HapticPulseUnity();
        }
    }
    void HapticPulseUnity()
    {
        InputDevice device = InputDevices.GetDeviceAtXRNode(XRNode.RightHand);
        HapticCapabilities capabilities;
        if (device.TryGetHapticCapabilities(out capabilities))
            if (capabilities.supportsImpulse)
                device.SendHapticImpulse(0, 0.3f, 2.5f);

        device = InputDevices.GetDeviceAtXRNode(XRNode.LeftHand);
        if (device.TryGetHapticCapabilities(out capabilities))
            if (capabilities.supportsImpulse)
                device.SendHapticImpulse(0, 0.3f, 2.5f);
    }

}
