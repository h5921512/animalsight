# AnimalSight

組員分工 劉志賢-企劃及場景設計 宋明彥-3D建模及影片剪輯 賴韋岑-3D建模級組視覺設計

## 主視覺及遊戲介紹

![主視覺海報](https://gitlab.com/h5921512/animalsight/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%871.PNG?inline=false)
![設計理念](https://gitlab.com/h5921512/animalsight/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%872.PNG?inline=false)
![背景故事](https://gitlab.com/h5921512/animalsight/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%873.PNG?inline=false)
![動物視角應用](https://gitlab.com/h5921512/animalsight/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%877.PNG?inline=false)
![老鷹視角應用](https://gitlab.com/h5921512/animalsight/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%878.PNG?inline=false)
![貓視角應用](https://gitlab.com/h5921512/animalsight/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%879.PNG?inline=false)
![蛇視角應用](https://gitlab.com/h5921512/animalsight/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%8710.PNG?inline=false)

## 遊戲玩法介紹

使用oculus 2 頭盔去遊玩，透過動物視角去解謎，手把上中指位置的按鈕為檢取物品，檢取法老王的飾品後，按下食指的按鈕可以切換不同視角的應用
